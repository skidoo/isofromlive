isofromlive
==================

This utility copies a running antiX live system, squashes the copy and generates
an iso image file suitable for use in the production of a customized live-CD.

Also, (untested) by feeding the iso to "live-usb-maker", the iso may be
suitable for creation of "from iso backup" customized live-USB.
